// TODO: Folderu /dist nie dolaczamy do repozytorium, zmniejsza to znaczaco wage repo a i tak kazdy zbuilduje sobie sam

var gulp = require("gulp");
var babel = require('gulp-babel');
var browserSync = require('browser-sync').create();
var sass = require("gulp-sass");
var sourcemaps = require("gulp-sourcemaps");
var autoprefix = require("gulp-autoprefixer");
var cleanCSS = require("gulp-clean-css");
var uglify = require("gulp-uglify");
var concat = require("gulp-concat");
var imgMin = require("gulp-imagemin");
var newer = require("gulp-newer");
var changed = require("gulp-changed");
var htmlmin = require("gulp-htmlmin");
var replace = require("gulp-html-replace");
var del = require("del");

const imgDest = 'dist/img'

gulp.task('sass', function () {
    return gulp.src("src/scss/**/*.scss")
        .pipe(sourcemaps.init())
        .pipe(sass().on("error", sass.logError))
        .pipe(autoprefix({
            browsers: ["last 3 versions"]
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest("src/css"))
        .pipe(browserSync.stream());
});

gulp.task("miniJS", function () {
    return gulp.src("src/js/**/*.js")
        .pipe(concat("app.js"))
        .pipe(gulp.dest("src/app"))
        .pipe(browserSync.stream());
});

gulp.task('serve', function () {
    browserSync.init({
        server: 'src'
    });

    gulp.watch("src/scss/**/*.scss", gulp.series(gulp.parallel("sass")));
    gulp.watch("src/js/**/*.js", gulp.parallel("miniJS"));
    gulp.watch("src/*.html").on("change", browserSync.reload);

});

gulp.task("css", function () {
    return gulp.src("src/css/**/style.css")
        .pipe(cleanCSS())
        .pipe(gulp.dest("dist/css"));
});

gulp.task("uglify", function () {
    return gulp.src("src/js/**/*.js")
        .pipe(concat("script.js"))
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(uglify())
        .pipe(gulp.dest("dist/js"));
});

gulp.task("img", function () {
    return gulp.src("src/img/**/*.{jpg,jpeg,png,btm}")
        .pipe(newer(imgDest))
        .pipe(imgMin())
        .pipe(gulp.dest("dist/img"));
})

gulp.task("html", function () {
    return gulp.src("src/*.html")
        .pipe(replace({
            "css": 'css/style.css',
            'js': 'js/script.js'
        }))
        .pipe(htmlmin({
            sortAttributes: true,
            sortClassName: true,
            collapseWhitespace: true

        }))
        .pipe(gulp.dest("dist"));
})
gulp.task("clean", function () {
    return del(['dist']);
});

gulp.task("build", gulp.series('clean', 'html', 'css', 'uglify', 'img', 'serve', 'sass'))

gulp.task('default', gulp.series( 'serve', 'sass'));