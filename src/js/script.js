var validatedFiles = [];
$(document).ready(function(){
    
    (function () {
        const content = $(".list-holder").html();
        $(".uk-offcanvas-bar").append(content);
    }());
    
    (function () {
        $('.menu-element, .switch-a, .notify').on('click', function (e) {
            e.preventDefault();
        });
    }());
    
    function appendIMG(src) {
        validatedFiles = [];
        $('.default-view, .input-section').hide();
        $(".drop-section").addClass("fullView");
        const img = $('<img class="file-upload-image"/>');
        img.attr("src",src);
        if($(".file-upload-image").length < 3){
            $(".img-display").append(img);
            $('.img-display').show();
        }
        else{
            displayAlert('Można dodać jedynie 3 zdjęcia!');
        }
    }

    const displayAlert = alertText => {
        // Creating Alert
        const container = document.createElement('div');
        container.classList.add('alert-container');
        const alert = document.createElement('div');
        alert.classList.add('alert');
        const text = document.createElement('span');
        text.classList.add('text');
        text.innerText = alertText;
        const exit = document.createElement('div');
        exit.classList.add('exit');
        exit.innerText = 'X';
        exit.addEventListener('click', e => {
            clearTimeout(timeOut)
            e.target.parentNode.parentNode.remove();
        })
        alert.appendChild(text);
        alert.appendChild(exit);
        container.appendChild(alert);
        document.body.appendChild(container)

        const timeOut = setTimeout(function(){
            $('.alert-container').remove();
        }, 5000);
        
    }

    const removeUpload = () => {
        $('.drop-input').val('');
        $(".drop-input, .drop-section").removeClass("draged");
        $('.img-display').hide();
        $(".drop-section").removeClass("fullView");
        $('.default-view, .input-section').show();
    }
    
    function readURL() {
        const _URL = window.URL || window.webkitURL;
        if ($(".file-upload-image").length > 0) {
            $(".file-upload-image").remove();
        }

        const input = $(".drop-input")["0"];

        if (input.files.length > 0 && validatedFiles.length === 0) {

            for(file of input.files){
                if(file.size <= 2097152){
                    const img = new Image();
                    
                    img.onload = function(){
                        if(this.width >= 600 && this.height >= 600){
                            validatedFiles.push(file);
                            appendIMG(this.src);
                            
                        }else{
                            displayAlert('Złe wymiary zdjęcia!');
                            removeUpload();
                        }
                    }
                    img.src = _URL.createObjectURL(file);
                } else{
                    displayAlert('Zbyt duży plik!')
                    removeUpload();
                } 
            }

        } else {
            removeUpload();
        }
    }


    $(".file-input").on("click", function () {
        $(".drop-input").click();
    
    });
    
    $(".drop-input").on("mouseenter", function () {
        $(".drop-section, .drop-input").addClass("draged");
    });
    
    $(".drop-input").on("mouseleave", function () {
        $(".drop-section, .drop-input").removeClass("draged");
    
    });

    $(".menu-element, .notify").click(function(){
        let href = this.getAttribute("href");
        let element = document.querySelector(href);
        $([document.documentElement, document.body]).animate({
            scrollTop: $(element).offset().top - 64
        }, 1000);
    });

    $(".add").on("click",function(){
        $(".active-switch").removeClass("active-switch");
        $(this).addClass("active-switch");
        if($(this).hasClass("addIMG")){
            $(".form-desc").removeClass("active-form");
            $(".form-img").addClass("active-form");
        }else{
            $(".form-desc").addClass("active-form");
            $(".form-img").removeClass("active-form");
        }
    });
    var input = document.querySelector(".drop-input");
    input.addEventListener("change", readURL);
});